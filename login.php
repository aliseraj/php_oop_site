<?php
use App\Controller\UserController;

require("./template/header.php");
$user = new UserController();
$user->login();
var_dump(request('remember'));
?>


    <!-- Page content-->
    <div class="container mt-5">
        <?php

        if ($flash->hasMessages($flash::ERROR)) {
            $flash->display();
        }
        ?>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <form action="/login.php" method="POST">

                        <div class="form-group">
                            <label>Email address</label>
                            <input type="text" name="email" class="form-control" placeholder="Enter email"
                                   value="<?= old('email') ?>">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Password"
                                   value="<?= old('password') ?>">
                        </div>
                        <div class="form-group">
                            <div><label><input type="checkbox" name="remember">remember me</label></div></div>


                        <button type="submit" class="btn btn-primary">login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer-->
<?php
require("./template/footer.php");
?>