<?php
use App\Controller\UserController;

require("./template/header.php");
$user = new UserController();
$user->register();
?>


    <!-- Page content-->
    <div class="container mt-5">
        <?php

        if ($flash->hasMessages($flash::ERROR)) {
            $flash->display();
        }
        ?>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <form action="/register.php" method="POST">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" placeholder="name..."
                                   value="<?= old('name') ?>">
                        </div>
                        <div class="form-group">
                            <label>Email address</label>
                            <input type="email" name="email" class="form-control" placeholder="Enter email"
                                   value="<?= old('email') ?>">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Password"
                                   value="<?= old('password') ?>">
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" name="confirm_password" class="form-control" placeholder="Password"
                                   value="<?= old('confirm_password') ?>">
                        </div>


                        <button type="submit" class="btn btn-primary">register</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer-->
<?php
require("./template/footer.php");
?>