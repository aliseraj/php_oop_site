<?php

use App\Helper\Request;
use App\Helper\Session;

function old($field)
{
    return request($field);
}

function request($field = null)
{
    $request = new Request();
    if (is_null($field))
        return $request;

    return $request->input($field);
}

function session($key = null)
{
    $session = new Session();
    if (is_null($key))
        return $session;
    return $session->get($key);
}
function cookie($key = null)
{
    $cookie = new \App\Helper\Cookie();
    if (is_null($key))
        return $cookie;
    return $cookie->get($key);
}
function random($length = 106){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

}


function redirect($param = null)
{
    if (is_null($param))
        $param = '/';
    header('location:' . $param);
    exit();

}