<?php namespace App\Controller;

use App\Helper\Auth;
use App\Model\User;
use Carbon\Carbon;

class UserController extends Controller
{
    public function register()
    {
        if(! request()->isPost())
            return;

        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:user',
            'password' => 'required|min:6|max:20',
            'confirm_password' => 'confirm:password'
        ];

        if(! $this->validation(request()->all() , $rules)) {
            return;
        }
        try {


            $user = new User();
            $success = $user->create([
                'name' => request('name'),
                'email' => request('email'),
                'password' => password_hash(request('password'), PASSWORD_BCRYPT, ['cost' => 12]),
                'created_at' => Carbon::now()
            ]);

            if ($success) {

                $this->flash->success('عضویت شما با موفقیت انجام شد');

                return redirect();
            }
        }catch (\Exception $e){
            die('Error : ' . $e->getMessage());
        }
    }

    public function login(){
        if (!request()->isPost())
            return;

        $rules=[
            'email' => 'required|email',
            'password' => 'required|min:6|max:20',
        ];
        if (!$this->validation(request()->all(),$rules))
            return;

        $user=(new User())->find('email',request('email'));
        $success = ($user)&&(password_verify(request('password'),$user->password));
        if (!$success)
            return($this->flash->error('ایمیل یا پسورد اشتباه است'));

        $remember = false;
        if (!empty(request('remember')))
            $remember = true;

        Auth::login($user,$remember);
            
        return redirect();
    }
}