<?php


namespace App\Contracts;


interface RequestInterface
{
    public function input($field,$post=true);
    public function all($post);
    public function isPost();

}