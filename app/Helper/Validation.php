<?php namespace App\Helper;

use App\Model\DB;

class Validation
{
    private $errors;
    private $data;

    public function make(array $data, array $rules)
    {
        $valid = true;
        $this->data = $data;

        foreach ($rules as $item => $ruleset) {
            $ruleset = explode('|', $ruleset);

            foreach ($ruleset as $rule) {
                $pos = strpos($rule, ":");

                if ($pos !== false) {
                    $parameter = substr($rule, $pos + 1);
                    $rule = substr($rule, 0, $pos);
                } else {
                    $parameter = "";
                }

                $MethodName = ucfirst($rule);
                $value = isset($data[$item]) ? $data[$item] : null;

                if (method_exists($this, $MethodName)) {

                    if ($this->{$MethodName}($item, $value, $parameter) == false) {
                        $valid = false;
                        break;
                    }
                }
            }


        }

        return $valid;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    private function required($item, $value)
    {
        if (strlen($value) == 0) {
            $this->errors[$item][] = "پرکردن  {$item}الزامیست";
            return false;
        }
        return true;

    }

    private function email($item, $value)
    {

        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $this->errors[$item][] = "فرمت ایمیل اشتباه است";
            return false;
        }
        return true;

    }

    private function min($item, $value, $param)
    {

        if (strlen($value) < $param) {
            $this->errors[$item][] = "طول {$item} نباید کمتر از {$param} باشد";
            return false;
        }
        return true;
    }

    private function max($item, $value, $param)
    {

        if (strlen($value) > $param) {
            $this->errors[$item][] = "طول {$item} نباید بیشتر از {$param} باشد";
            return false;
        }
        return true;
    }

    private function confirm($item, $value, $param)
    {
        $original = isset($this->data[$item]) ? $this->data[$item] : null;
        $confirm = isset($this->data[$param]) ? $this->data[$param] : null;

        if ($original != $confirm) {
            $this->errors[$item][] = "فیلد {$item} با {$param} تطابق ندارد";
            return false;
        }
        return true;
    }

    public function unique($item , $value , $parameter)
    {
    $db = new DB();
    if (is_null($parameter))
        return false;

    $db->from($parameter);
    if($db->find($item,$value) !=false){
        $this->errors[$item][]="مقدار تکراری";
        return false;
         }
    return true;
    }
}

