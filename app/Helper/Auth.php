<?php namespace App\Helper;

use App\Contracts\AuthInterface;
use App\model\User;

class Auth implements AuthInterface{
    public static function login ($user , $remember =false){
        if($remember ==true){
            $rememberToken = random();
            (new User())->update($user->id,[
                'remember_token'=>$rememberToken
            ]);
            cookie()->set('remember_token',$rememberToken);
        }
        return true;
    }

    public static function check(){

    }

    public static function logout(){

    }

    public static function user(){
        
    }
}